import unittest, datetime
import stmo_rapm_stepResult as re

seg_and_risk_adjust = {
    "Commercial Real Estate": [550.11, 120.48],
    "Accommodation and Food Services": [1286.694],
    "Industrial": [14783.14],
    "SME": [100],
    "Third Party Mortgages": [4567],
    "Broker Mortgage": [123485, 1561],
    "Dealer Loan": [0, 8662],
    "Other Personal Loans": [1238, 0, 12.28],
    "Residential Mortgage": [546, 89.21],
    "HELOC": [],
    "Retail Revolving LOC": [128.1, 58, 9]
}

seg_and_raroc = {
    "Commercial Real Estate": [1.23, 0.42],
    "Accommodation and Food Services": [0.98],
    "Industrial": [0.56],
    "SME": [1],
    "Third Party Mortgages": [0.62],
    "Broker Mortgage": [1.58, 0.48],
    "Dealer Loan": [0, 0.95],
    "Other Personal Loans": [1.28, 0, 0.28],
    "Residential Mortgage": [0.56, 1.21],
    "HELOC": [],
    "Retail Revolving LOC": [0.2, 1.2, 0.8]

}

seg_hurdle_rate_comp = {
    "Commercial Real Estate": ["No", "Yes", "No", "No"],
    "Accommodation and Food Services": ["Yes"],
    "Industrial": ["No"],
    "SME": ["Yes"],
    "HELOC": [],
}

sum_cal = [10, 351, 1354.1]
sum_cal2 = []

list_raroc = [1.23, 0.99, 1]
list_risk = [1002.23, 350.14, 98.15]

list_risk2 = [1002.23, 350.14, 98.15, 123]

list_risk3 = []
list_raroc2 = []


class TestRapm(unittest.TestCase):
    def test_Calculate_sum(self):
        self.assertAlmostEqual(re.Calculate_Sum("SME", seg_and_risk_adjust), 100)
        self.assertEqual(re.Calculate_Sum(None, seg_and_risk_adjust), 0)
        self.assertAlmostEqual(re.Calculate_Sum("HELOC", seg_and_risk_adjust), 0)
        self.assertEqual(re.Calculate_Sum("auhf", seg_and_risk_adjust), 0)
        self.assertEqual(re.Calculate_Sum("", seg_and_risk_adjust), 0)
        self.assertEqual(re.Calculate_Sum("SME", None), 0)

    def test_Average_RAROC(self):
        self.assertAlmostEqual(re.Average_RAROC("Commercial Real Estate", seg_and_raroc), 0.825)
        self.assertAlmostEqual(re.Average_RAROC("Broker Mortgage", seg_and_raroc), 1.03)
        self.assertAlmostEqual(re.Average_RAROC("HELOC", seg_and_raroc), 0)
        self.assertEqual(re.Average_RAROC("awt", seg_and_raroc), 0)
        self.assertEqual(re.Average_RAROC(None, seg_and_raroc), 0)
        self.assertEqual(re.Average_RAROC("SME", None), 0)
        self.assertEqual(re.Average_RAROC("", seg_and_raroc), 0)

    def test_no_trans(self):
        self.assertEqual(re.No_of_transactions_below_hurdle_rate("Commercial Real Estate", seg_hurdle_rate_comp), 3)
        self.assertEqual(re.No_of_transactions_below_hurdle_rate(None, seg_hurdle_rate_comp), None)
        self.assertEqual(re.No_of_transactions_below_hurdle_rate("Commercial Real Estate", None), None)
        self.assertEqual(re.No_of_transactions_below_hurdle_rate("HELOC", seg_hurdle_rate_comp), None)
        self.assertEqual(re.No_of_transactions_below_hurdle_rate("asd", seg_hurdle_rate_comp), None)

    def test_sum_cal(self):
        self.assertAlmostEqual(re.Sum_Calculation(sum_cal), 1715.1)
        self.assertEqual(re.Sum_Calculation(None), 0)
        self.assertEqual(re.Sum_Calculation(sum_cal2), 0)

    def test_sum_avg_raroc(self):
        self.assertAlmostEqual(re.Sum_avg_RAROC(list_raroc, list_risk), 1677.5315)
        self.assertAlmostEqual(re.Sum_avg_RAROC(list_raroc, list_risk2), 0)
        self.assertAlmostEqual(re.Sum_avg_RAROC(None, list_risk), 0)
        self.assertAlmostEqual(re.Sum_avg_RAROC(list_raroc, None), 0)
        self.assertAlmostEqual(re.Sum_avg_RAROC(list_raroc2, list_risk), 0)
        self.assertAlmostEqual(re.Sum_avg_RAROC(list_raroc, list_risk3), 0)


if __name__ == '__main__':
    unittest.main()
