"""
This function calculate the sum of any required information in the result table of of any segment in the excel sheet
:param segment: is a string that is the name of the segment found in the Excel sheet
:param seg_and_the_element_to_calculate: is the dictionary in which the key are the segment and the is an array
where the elements are the specific information that we want to find
Expected dictionary format: seg_and_risk_adjust = {
    "Commercial Real Estate": [550.11, 120.48],
    "Accommodation and Food Services": [1286.694],
    "Industrial": [14783.14]}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the sum what we want to find
"""


def Calculate_Sum(segment: str, seg_and_the_element_to_calculate: dict):
    sum_scaled = 0
    if segment is not None and seg_and_the_element_to_calculate is not None and segment in seg_and_the_element_to_calculate and segment != "":
        for key in seg_and_the_element_to_calculate:
            if key == segment:
                for item in seg_and_the_element_to_calculate[key]:
                    if item is not None:
                        sum_scaled += item
        return round(sum_scaled, 1)
    else:
        return sum_scaled


'''
This function calculate the average RAROC of a specific segment
:param segment: is a string that is the name of the segment found in the Excel sheet
:param seg_and_raroc: is the dictionary in which the key are the segment and the is an array 
where the elements are the raroc value found in step 2 sheet
Expected dictionary format: seg_and_raroc = {
    "Commercial Real Estate": [1.23, 0.42],
    "Accommodation and Food Services": [0.98],
    "Industrial": [0.56]}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the average raroc value
'''


def Average_RAROC(segment: str, seg_and_raroc: dict):
    sum_scaled = 0
    count = 0
    if segment is not None and seg_and_raroc is not None and segment in seg_and_raroc and segment != "":
        for key in seg_and_raroc:
            if key == segment:
                if len(seg_and_raroc[key]) == 0:
                    return 0
                else:
                    for item in seg_and_raroc[key]:
                        if item is not None:
                            sum_scaled += item
                            count += 1
        return sum_scaled / count
    else:
        return 0


'''
This function calculate the number of time that the hurdle rate compliance is a "No" for a specific segment
:param segment: is a string that is the name of the segment found in the Excel sheet
:param seg_and_hurdle_rate_comp_concatenate: is the dictionary in which the key are the segment and the is an array 
where the elements are hurdle rate compliance
Expected dictionary format: seg_hurdle_rate_comp = {
    "Commercial Real Estate": ["No", "Yes", "No", "No"],
    "Accommodation and Food Services": ["Yes"]
    }
The element inside the array in each key come from hurdle rate compliance column in the Excel sheet
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return: return the counter of the number of time the compliance rate is No for a specific segment
'''


def No_of_transactions_below_hurdle_rate(segment: str, seg_and_hurdle_rate_comp_concatenate: dict):
    counter = 0
    if segment is not None and seg_and_hurdle_rate_comp_concatenate is not None and segment in \
            seg_and_hurdle_rate_comp_concatenate and segment != "":
        for key in seg_and_hurdle_rate_comp_concatenate:
            if key == segment and len(seg_and_hurdle_rate_comp_concatenate[key]) != 0:
                for item in seg_and_hurdle_rate_comp_concatenate[key]:
                    if item is not None and item == "No":
                        counter += 1
                return counter
            else:
                return None
        return counter
    else:
        return None


'''
This function calculate the sum of all the segment's values displayed in the Excel sheet which is the same type of value
:param seg_and_hurdle_rate_comp_concatenate: is an array where the elements are the values of the specific type of data
Expected list format: sum_cal = [10, 351, 1354.1]
The way to create a list is exactly the same in the format, but the values are decided by
the user, the length of each value is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the list and customize it to match
their need.
EX: name_of_list = [element, element,...]
:return the sum of all the element in the array
'''


def Sum_Calculation(array_of_calculation_results: list):
    result = 0
    if array_of_calculation_results is not None and len(array_of_calculation_results) != 0:
        for item in array_of_calculation_results:
            if item is not None:
                result += item
        return result
    else:
        return result


'''
This function calculate the dot product of the 2 array which contain the values of 
average RAROC and average risked adjustment
:param array_of_risk_adj: is an array where the elements are the risk adjusted values
:param array_of_avg_raroc: is an array where the elements are the average RAROC values
Expected list format: list_risk2 = [1002.23, 350.14, 98.15, 123]
The way to create a list is exactly the same in the format, but the values are decided by
the user, the length of each value is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the list and customize it to match
their need.
EX: name_of_list = [element, element,...]
The lists must have the same length or else the function won't work
:return the dot product of the 2 arrays
'''


def Sum_avg_RAROC(array_of_avg_raroc: list, array_of_risk_adj: list):
    result = 0
    if array_of_avg_raroc is not None and array_of_risk_adj is not None and len(array_of_avg_raroc) != 0 and \
            len(array_of_risk_adj) != 0 and len(array_of_avg_raroc) == len(array_of_risk_adj):
        for i in range(len(array_of_avg_raroc)):
            if array_of_avg_raroc[i] is not None or array_of_risk_adj[i] is not None:
                result += array_of_avg_raroc[i] * array_of_risk_adj[i]
        return result
    else:
        return result




