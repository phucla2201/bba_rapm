import math
from scipy.special import ndtri
from scipy.stats import norm
import yearfrac
import datetime

z_value = 2.748

'''
This function find the correct asset class map for different segment
:param segment_name: the name of the segment obtained from the excel sheet
:param fcu_basel: is the dictionary with the keys are the segment name and value are the asset classes
Expected dictionary format: FCU_basel = {
    'Commercial Real Estate': 'HVCRE',
    'Accommodation and Food Services': 'Corporate'}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the name of the asset class
'''


def asset_class_map(segment_name: str, fcu_basel: dict):
    if segment_name == "" or segment_name is None or fcu_basel is None or segment_name not in fcu_basel:
        return ""
    else:
        return fcu_basel[segment_name]


'''
This function find the PD mapping based on rating grade that are number only
:param rating_grade: this is the number of rating grade obtained from the excel sheet
:param rating_mean: this is the dictionary where for each rating grade, there is a corresponding value, and the key are 
just integer, and the rating grade will be approximate to the closest number
Expected dictionary format: Rating_mean_nums = {
    324: 0.37960,
    374: 0.23304,
    411: 0.17635,
    437: 0.13110,
    464: 0.09611,
    488: 0.06970,
    514: 0.05015,
    538: 0.03587}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the pd number
'''


def PD_mapping_nums(rating_grade: float, rating_mean: dict):
    first_key = True
    diff = 0
    value = 0
    if rating_grade == 0 or rating_grade is None or rating_mean is None:
        return 0.0005
    else:
        for key in rating_mean.keys():
            if first_key:
                diff = rating_grade - key
                first_key = False
            else:
                if diff > rating_grade - key > 0:
                    diff = rating_grade - key
                    value = rating_mean[key]
                elif diff > key - rating_grade > 0:
                    diff = key - rating_grade
                    value = rating_mean[key]
    return value


'''
This function find the PD mapping based on rating grade that are string only
:param rating_grade: this is the string of rating grade
:param rating_mean: this is the dictionary where for each rating grade, there is a corresponding value, and the key are 
just string, and the rating grade will be exact
Expected dictionary format: Rating_mean_str = {
    "C1": 0.0001,
    "C2": 0.0005}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the pd number
'''


def PD_mapping_str(rating_grade: str, rating_mean: dict):
    if rating_mean is None or rating_grade is None or rating_grade not in rating_mean or rating_grade == "":
        return 0.0005
    else:
        return rating_mean[rating_grade]


'''
This function find the lgd value from the facility type 
:param facility_type: is the name of facility type 
:param senior_rate: is the dictionary where each facility type is a key and each has a corresponding value
Expected dictionary format: Senior_rate = {
    'Senior Unsecured': 0.45,
    'Senior Secured, Collateral - Physical': 0.40}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the value ratio correspond to the name of facility type
'''


def LGD(facility_type: str, senior_rate: dict):
    if senior_rate is None or facility_type is None or facility_type == "" or facility_type not in senior_rate:
        return ""
    else:
        return senior_rate[facility_type]


'''
This function is to find the correct correlation ration depends on the basel classification and the pd mapping value
:param basel_classification: the name of the asset class 
:param pd_mapping: is the float value of pd mapping 
:return the corresponding value based on the name of asset class
'''


def Correlation(basel_classification: str, pd_mapping: float):
    if basel_classification == 'RRE':
        return 0.15
    elif basel_classification == 'QRRE' or basel_classification is None or basel_classification == "":
        return 0.04
    elif basel_classification == 'SME' and pd_mapping is not None:
        calculated = 0.12 * (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50)) \
                     + 0.24 * (1 - (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50))) - 0.04 * (1 - (10 - 5) / 45)
        return calculated
    elif basel_classification == 'Corporate' and pd_mapping is not None:
        return 0.12 * (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50)) + 0.24 * (
                1 - (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50)))
    elif basel_classification == 'HVCRE' and pd_mapping is not None:
        return 0.12 * (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50)) + 0.3 * (
                1 - (1 - math.exp(-50 * pd_mapping)) / (1 - math.exp(-50)))
    elif basel_classification == 'Retail' and pd_mapping is not None:
        return 0.03 * (1 - math.exp(-35 * pd_mapping)) / (1 - math.exp(-35)) + 0.24 * (
                1 - (1 - math.exp(-35 * pd_mapping)) / (1 - math.exp(-35)))
    else:
        return 0.04


'''
This function find the value of maturity adjustment based on the pd value
:param pd_mapping: is the float value of pd mapping 
:return the result from the calculation to find the maturity adjustment based on the pd value
'''


def Maturity_adjustment(pd_mapping: float):
    if pd_mapping is not None and pd_mapping > 0:
        return math.pow((0.11852 - 0.05478 * math.log(pd_mapping)), 2)
    else:
        return ""


'''
This function find the expected lost value based on the pd and lgd value
:param pd_mapping: is the float value of pd mapping 
:param ldg: is the float value of LDG 
:return the result from the multiplying pd and lgd values together, otherwise return 0
'''


def Expected_loss(pd_mapping: float, lgd: float):
    if pd_mapping is not None and lgd is not None:
        return pd_mapping * lgd
    else:
        return 0


'''
This function calculate the EL value based on the expected loss value and credit equivalent value
:param expected_loss: this is the expected loss value 
:param credit_equi: this is the credit equivalent value 
:return the result of the calculation based on these 2 value round up to 2 decimals, otherwise, return null if anything 
went wrong
'''


def EL(expected_loss: float, credit_equi: float):
    if expected_loss is not None and credit_equi is not None:
        return round(expected_loss * credit_equi, 2)
    else:
        return None


'''
This function calculate the G(PD) value based just on the pd value 
:param pd_mapping: is the float value of pd mapping 
:return the result of the calculation based on the pd value
'''


def G_PD(pd_mapping: float):
    if pd_mapping is not None and pd_mapping > 0:
        return ndtri(pd_mapping)
    else:
        return ""


'''
This function calculate the maturity adjustment ecap value based on the maturity adjustment and maturity values
:param maturity_adjustment: is the float value of maturity adjustment 
:param maturity: is the float value of maturity 
:return the result from the calculation based on the 2 mentioned above values
'''


def Maturity_Adjustment_Ecap(maturity_adjustment: float, maturity: float):
    if maturity_adjustment is not None and maturity is not None:
        return 1 + (maturity - 2.5) * maturity_adjustment
    else:
        return ""


'''
This function calculate the N value based on G(PD) and Correlation values
:param g_pd: is the float value of G(PD) 
:param correlation: is the float value of Correlation 
:return the result from the calculation based on the 2 mentioned above values
'''


def N(g_pd: float, correlation: float):
    if g_pd is not None and correlation is not None:
        return norm.cdf(
            (math.pow(1 - correlation, -0.5)) * g_pd + (math.pow((correlation / (1 - correlation)), 0.5)) * z_value)
    else:
        return ""


'''
This function calculate the VaR value based on N and LGD values
:param n: is the float value of N 
:param lgd: is the float value of LGD 
:return the result from the calculation based on the 2 mentioned above values
'''


def VaR(n: float, lgd: float):
    if n is not None and lgd is not None:
        return n * lgd
    else:
        return ""


'''
This function calculate the Ecap % value based on VaR and Expected Loss values
:param var: is the float value of VaR 
:param expected_loss: is the float value of Expected Loss 
:return the result from the calculation based on the 2 mentioned above values
'''


def Ecap_pct(var: float, expected_loss: float):
    if var is not None and expected_loss is not None and expected_loss != 0 and var != 0:
        return var - expected_loss
    else:
        return ""


'''
This function calculate the Ecap value based on Ecap % and Credit Equivalent values
:param ecap_pct: is the float value of Ecap % 
:param credit_equi: is the float value of Credit Equivalent 
:return the result from the calculation based on the 2 mentioned above values
'''


def Ecap(ecap_pct: float, credit_equi: float):
    if ecap_pct is not None and credit_equi is not None:
        return round(ecap_pct * credit_equi, 2)
    else:
        return None


'''
This function calculate the Adjustment Factor value based on Maturity Adjustment Ecap and Maturity Adjustment values
:param maturity_adjustment_ecap: is the float value of Maturity Adjustment_Ecap 
:param maturity_adjustment: is the float value of Maturity Adjustment 
:return the result from the calculation based on the 2 mentioned above values
'''


def Adjustment_Factor(maturity_adjustment_ecap: float, maturity_adjustment: float):
    if maturity_adjustment is not None and maturity_adjustment_ecap is not None:
        return math.pow((1 - 1.5 * maturity_adjustment), -1) * maturity_adjustment_ecap
    else:
        return 1


'''
This function calculate the MtM Capital % value based on Asset Class Mapping, Ecap % and Adjustment Factor values
:param asset_class_mapping: is the name of the Asset Class
:param ecap_pct: is the float value of Ecap % 
:param adjustment_factor: is the float value of Adjustment Factor 
:return the result from the calculation based on the 3 mentioned above values
'''


def MtM_Capital_pct(asset_class_mapping: str, ecap_pct: float, adjustment_factor: float):
    if asset_class_mapping == 'RRE' and ecap_pct is not None:
        return ecap_pct
    else:
        if ecap_pct is not None and adjustment_factor is not None:
            return ecap_pct * adjustment_factor
        else:
            return 0


'''
This function calculate the MtM Capital value based on MtM Capital % and Credit Equivalent values
:param mtm_capital_pct: is the float value of MtM Capital % 
:param credit_equi: is the float value of Credit Equivalent 
:return the result from the calculation based on the 2 mentioned above values
'''


def MtM_Capital(mtm_capital_pct: float, credit_equi: float):
    if mtm_capital_pct is not None and credit_equi is not None:
        return round(mtm_capital_pct * credit_equi, 2)
    else:
        return None


'''
This function calculate the Granularity value based on MtM Capital % and Expected Loss values
:param mtm_capital_pct: is the float value of MtM Capital % 
:param expected_loss: is the float value of Expected Loss 
:return the result from the calculation based on the 2 mentioned above values
'''


def Granularity(mtm_capital_pct: float, expected_loss: float):
    if mtm_capital_pct is not None and mtm_capital_pct != 0 and expected_loss is not None:
        return (1 / (2 * mtm_capital_pct)) * (4.83 * (mtm_capital_pct + expected_loss) - mtm_capital_pct)
    else:
        return 0


'''
This function calculate the HHI ratio based on the outstanding values provide in the form of a list
:param outstanding_arr: this is a list that contain outstanding values
Expected list format: arr = [2254.98, 0, 22549.32, 0, 563.7, 0, 0, 0, 0, 20290.89]
The elements inside the list are the outstanding values
The way to create a list is exactly the same in the format, but the values are decided by
the user, the length of each value is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the list and customize it to match
their need.
EX: name_of_list = [element, element,...]
:return the result of sum of the square of the outstanding values
'''


def HHI(outstanding_arr: list):
    sum = 0
    sum_sqr = 0
    if outstanding_arr is not None and len(outstanding_arr) != 0:
        for item in outstanding_arr:
            sum += item
        for value in outstanding_arr:
            sum_sqr += math.pow((value / sum), 2)
        return sum_sqr
    else:
        return sum_sqr


'''
This function calculate the MtM + GA Capital % value based on MtM Capital %, LGD, Granularity and HHI values
:param mtm_capital_pct: is the float value of MtM Capital % 
:param lgd: is the float value of LGD
:param granularity: is the float value of Granularity 
:param hhi: is the float value of HHI
:return the result from the calculation based on the 4 mentioned above values
'''


def MtM_plus_GA_Capital_pct(mtm_capital_pct: float, lgd: float, granularity: float, hhi: float):
    if mtm_capital_pct is not None and lgd is not None and granularity is not None and hhi is not None:
        return (lgd * granularity * hhi) + mtm_capital_pct
    else:
        if mtm_capital_pct is None:
            return ""
        else:
            return "" + str(mtm_capital_pct)


'''
This function calculate the MtM + GA Capital value based on MtM Capital % and Credit Equivalent values
:param mtm_capital_pct: is the float value of MtM Capital % 
:param credit_equi: is the float value of Credit Equivalent 
:return the result from the calculation based on the 2 mentioned above values rounded to 2 decimals
'''


def MtM_plus_GA_Captial(mtm_plus_ga_capital_pct: float, credit_equi: float):
    if mtm_plus_ga_capital_pct is not None and credit_equi is not None:
        return round(mtm_plus_ga_capital_pct * credit_equi, 2)
    else:
        return None


'''
This function calculate the Remaining Maturity value from the provided dates
:param initial_date: is the starting date
:param expiry_date: is the end date
Expected datetime format: datetime.datetime(2019, 3, 31) 
It is important that the datetime module is imported before using datetime
:return the time in between the end date and the initial date rounded to 2 decimals
'''


def Remaining_Maturity(initial_date: datetime.datetime, expiry_date: datetime.datetime):
    if initial_date is not None and expiry_date is not None:
        return round(yearfrac.yearfrac(initial_date, expiry_date), 2)
    else:
        return ""


'''
This function concatenate the Facility definition and Committed strings together
:param facility_def: is the string of Facility definition 
:param Committed: is the string of Committed (Yes/No) 
:return the result from the concatenate from the 2 strings above
'''


def Facility_Concatenate(facility_def: str, committed: str):
    if facility_def is not None and committed is not None:
        return facility_def + " " + committed
    else:
        if facility_def is None and committed is not None:
            return committed
        elif committed is None and facility_def is not None:
            return facility_def
        else:
            return ""


'''
This function calculate the Off Balance Sheet Exposure at Default value based on Facility Concatenate string, Remaining 
Maturity and Undrawn Amount values
:param facility_concatenate: is the string of Facility Concatenate
:param remaining_maturity: is the float value of the Remaining Maturity 
:param undrawn_amount: is the float value of Undrawn Amount 
:return the result from the calculation based on the 3 mentioned above parameters
'''


def Off_balance_sheet_exposure_at_default(facility_concatenate: str, remaining_maturity: float, undrawn_amount: float):
    if facility_concatenate is not None and facility_concatenate == "Exposure I - On Balance Sheet Yes":
        if remaining_maturity is not None and undrawn_amount is not None and \
                ((facility_concatenate == "Exposure I - On Balance Sheet Yes" and remaining_maturity > 1) or
                 (facility_concatenate == "Exposure I - On Balance Sheet Yes" and remaining_maturity <= 1)):
            return undrawn_amount * 0.5
        else:
            if remaining_maturity is None and undrawn_amount is not None:
                return undrawn_amount * 0.2
            else:
                return 0
    else:
        return 0


'''
This function calculate the On balance sheet Exposure at Default based on the Facility Definition string, Outstanding 
value and and Exposure % dictionary
:param facility_concatenate: is the string of Facility Concatenate
:param outstanding: is the float value of the Outstanding 
:param exposure-pct: is dictionary where the keys are the Facility Concatenate and the corresponding values for each key
Expected dictionary format: exposure_pct = {
    'Exposure I - On Balance Sheet': 1,
    'Exposure I - Off Balance Sheet': 1,
    'Exposure II': 0.5} 
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the result from the calculation
'''


def On_balance_sheet_exposure_at_default(facility_definition: str, outstanding: float, exposure_pct: dict):
    if facility_definition is not None and facility_definition != "" and exposure_pct is not None \
            and facility_definition in exposure_pct and outstanding is not None:
        return exposure_pct[facility_definition] * outstanding
    else:
        return None


'''
This function calculate the Credit Equivalent value based on Off balance sheet Exposure at Default and 
On balance sheet Exposure at Default values
:param off_balance: is the float value of Off balance sheet Exposure at Default
:param expected_loss: is the float value of On balance sheet Exposure at Default
:return the result from the calculation based on the 2 mentioned above values
'''


def Credit_Equivalent_Credit_Risk(off_balance: float, on_balance: float):
    if off_balance is not None and on_balance is not None:
        return off_balance + on_balance
    else:
        return None


'''
This function calculate the CE Concentration Risk value based on MtM + GA Capital values
:param mtm_ga_capital: is the float value of MtM + GA Capital % 
:return the result from the calculation based on the mentioned above value
'''


def CE_Concentration_Risk(mtm_ga_capital: float):
    if mtm_ga_capital is not None:
        return round(mtm_ga_capital * 12.5, 2)
    else:
        return None


'''
This function calculate the Operational Risk Capital value based on Yield $ values
:param yield_in_dollar: is the float value of Yield $
:return the result from the calculation based on the mentioned above value
'''


def Operational_Risk_Capital(yield_in_dollar: float):
    if yield_in_dollar is not None:
        return round(yield_in_dollar * 0.15 * 0.08, 2)
    else:
        return None


'''
This function calculate the CE op.risk value based on Operational Risk capital value
:param operational_risk_capital: is the float value of Operational Risk capital value 
:return the result from the calculation based on the mentioned above value
'''


def CE_op_risk(operational_risk_capital: float):
    if operational_risk_capital is not None:
        return round(operational_risk_capital * 12.5, 2)
    else:
        return None


'''
This function calculate the Scaled CE value based on Credit Equivalent, CE Concentration Risk and CE op.risk values
:param credit_equi: is the float value of Credit Equivalent
:param ce_concen_risk: is the float value of CE Concentration Risk
:param ce_op_risk: is the float value of CE op.risk 
:return the result from the calculation based on the 3 mentioned above values
'''


def Scaled_CE(credit_equi: float, ce_concen_risk: float, ce_op_risk: float):
    if credit_equi is not None and ce_concen_risk is not None and ce_op_risk is not None:
        return round(credit_equi + ce_concen_risk + ce_op_risk, 2)
    else:
        return None


'''
This function calculate the Yield $ value based on Credit Equivalent and Yield (%) values
:param credit_equi: is the float value of Credit Equivalent
:param yield_pct: is the float value of Yield (%)
:return the result from the calculation based on the 2 mentioned above values
'''


def Yield_dollar(credit_equi: float, yield_pct: float):
    if credit_equi is not None and yield_pct is not None:
        return round(credit_equi * yield_pct, 2)
    else:
        return None


'''
This function calculate the Funding cost (%) value based on the Facility Definition string, Remaining Maturity value and
Mins cost of Fund dictionary
:param facility_def: is the string of Facility Definition
:param remaining_maturity: is the float of Remaining Maturity
:param mins_cost_of_fundL is the dictionary where each key has a different ratio
Expected dictionary format: mins_cost_of_fund = {
    0: 0,
    0.5: 0.001,
    1: 0.002}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the corresponding value of the dictionary when identified the correct key
'''


def Funding_cost(facility_def: str, remaining_maturity: float, mins_cost_of_fund: dict):
    if facility_def is not None and facility_def == 'Exposure I - On Balance Sheet' and remaining_maturity is not None \
            and mins_cost_of_fund is not None:
        if remaining_maturity >= 15:
            return mins_cost_of_fund[15]
        elif 15 > remaining_maturity >= 10:
            return mins_cost_of_fund[10]
        elif 10 > remaining_maturity >= 7:
            return mins_cost_of_fund[7]
        elif 7 > remaining_maturity >= 5:
            return mins_cost_of_fund[5]
        elif 5 > remaining_maturity >= 3:
            return mins_cost_of_fund[3]
        elif 3 > remaining_maturity >= 1:
            return mins_cost_of_fund[1]
        elif 1 > remaining_maturity >= 0.5:
            return mins_cost_of_fund[0.5]
        elif remaining_maturity < 0.5:
            return mins_cost_of_fund[0]
    else:
        return 0


'''
This function calculate the Funding Cost ($) value based on Funding Cost (%) and Scaled CE values
:param funding_cost: is the float value of Funding Cost (%)
:param scaled_ce: is the float value of Scaled CE 
:return the result from the calculation based on the 2 mentioned above values
'''


def Funding_Cost_Dollar(funding_cost: float, scaled_ce: float):
    if funding_cost is not None and scaled_ce is not None:
        return round(funding_cost * scaled_ce, 2)
    else:
        return None


'''
This function calculate the Scaled Expected Loss value based on Scaled CE and Expected Loss values
:param scaled_ce: is the float value of Scaled CE 
:param expected_loss: is the float value of Expected Loss
:return the result from the calculation based on the 2 mentioned above values
'''


def Scaled_Expected_Loss(scaled_ce: float, expected_loss: float):
    if scaled_ce is not None and expected_loss is not None and scaled_ce != "" and expected_loss != "":
        return round(scaled_ce * expected_loss, 2)
    else:
        return ""


'''
This function calculate the Capital Required value based on Scaled CE value
:param scaled_ce: is the float value of Scaled CE 
:return the result from the calculation based on the mentioned above value
'''


def Capital_Required(scaled_ce: float):
    if scaled_ce is not None and scaled_ce != 0:
        return round(scaled_ce * 0.08, 2)
    else:
        return ""


'''
This function calculate the Capital Required Scaled value based on Committed string, Remaining Maturity 
and Capital Required values and Mins Capital Scaling Factor dictionary.
:param committed: is the string of Committed
:param remaining_maturity: is the float value of Remaining Maturity
:param capital_required: is the float value of Capital Required
:param mins_capital_scaling_factor: is the dictionary where each key is the approximate remaining maturity time and each
the key has a value correspond to it 
Expected dictionary format: mins_capital_scaling_factor = {
    0: 1,
    0.5: 1,
    1: 1,
    3: 1}
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the result from the calculation based on the 4 mentioned above parameters
'''


def Capital_Required_Scaled(commited: str, remaining_maturity: float, capital_required: float,
                            mins_capital_scaling_factor: dict):
    if commited is not None and remaining_maturity is not None and capital_required is not None and \
            mins_capital_scaling_factor is not None:
        if commited == "Yes":
            if remaining_maturity >= 15:
                return round(mins_capital_scaling_factor[15] * capital_required, 2)
            elif 15 > remaining_maturity >= 10:
                return round(mins_capital_scaling_factor[10] * capital_required, 2)
            elif 10 > remaining_maturity >= 7:
                return round(mins_capital_scaling_factor[7] * capital_required, 2)
            elif 7 > remaining_maturity >= 5:
                return round(mins_capital_scaling_factor[5] * capital_required, 2)
            elif 5 > remaining_maturity >= 3:
                return round(mins_capital_scaling_factor[3] * capital_required, 2)
            elif 3 > remaining_maturity >= 1:
                return round(mins_capital_scaling_factor[1] * capital_required, 2)
            elif 1 > remaining_maturity >= 0.5:
                return round(mins_capital_scaling_factor[0.5] * capital_required, 2)
            elif remaining_maturity < 0.5:
                return round(mins_capital_scaling_factor[0] * capital_required, 2)
        else:
            return round(capital_required, 2)
    else:
        return ""


'''
This function calculate the Capital Cost value based on Capital Required Scaled and Hurdle Rate values
:param capital_required_scaled: is the float value of Capital Required Scaled
:param hurdle_rate: is the float value of Hurdle Rate
:return the result from the calculation based on the 2 mentioned above values rounded to 2 decimals
'''


def Capital_cost(capital_required_scaled: float, hurdle_rate: float):
    if capital_required_scaled is not None and hurdle_rate is not None:
        if capital_required_scaled == "" or hurdle_rate == "":
            return ""
        else:
            return round(capital_required_scaled * hurdle_rate, 2)
    else:
        return ""


def Cost_of_servicing(yield_in_dollar: float):
    """
    This function calculate the cost of servicing bassed on the yield in dollar valaue
    :param yield_in_dollar: is the float value of Capital Cost
    :return: return the result from the calculation based on the mentioned above value
    """
    if yield_in_dollar is not None:
        return yield_in_dollar * 0.3
    else:
        return 0


'''
This function calculate the Risk_adjusted_return value based on Yield $, Funding Cost ($), Scaled Expected Loss and
and Capital Cost values
:param yield_in_dollar: is the float value of Yield $
:param funding_cost_dollar: is the float value of Funding Cost ($)
:param scaled_expected_loss: is the float value of Scaled Expected Loss
:param capital_cost: is the float value of Capital Cost
:param cost_of_servicing: is the float value of Cost of Servicing
:return the result from the calculation based on the 4 mentioned above values rounded to 2 decimals
'''


def Risk_adjusted_margin(yield_in_dollar: float, funding_cost_dollar: float, scaled_expected_loss: float,
                         capital_cost: float, cost_of_servicing: float):
    if yield_in_dollar is not None and funding_cost_dollar is not None and \
            scaled_expected_loss is not None and capital_cost is not None and cost_of_servicing is not None:
        return round(yield_in_dollar - funding_cost_dollar - scaled_expected_loss - capital_cost - cost_of_servicing, 2)
    else:
        return ""


'''
This function calculate the RAROC value based on Risk Adjusted Return and Capital Required Scaled values
:param risked_adjusted_margin: is the float value of Risk-adjusted return
:param capital_required_scaled: is the float value of Capital Required Scaled
:return the result from the calculation based on the 2 mentioned above values rounded to 2 decimals
'''


def RAROC(risked_adjusted_margin: float, capital_required_scaled: float):
    if risked_adjusted_margin is not None and capital_required_scaled is not None:
        return risked_adjusted_margin / capital_required_scaled
    else:
        return 0


'''
This function calculate the Hurdle Rate based on the name of Segment and the Segment Hurdle Rate dictionary
:param segment: is the string of the name of Segment
:param segment_hurdle_rate: is the dictionary where the key is the name of segment and each has a corresponding value
Expected dictionary format: segment_hurdle_rate = {
    'Commercial Real Estate': 0.16,
    'Accommodation and Food Services': 0.18,
    'Broker Mortgage': 0.12,
    'Dealer Loan': 0.12} 
The way to create a dictionary is exactly the same in the format, but the number of keys and value pairs are decided by
the user, especially the value, the length of the number is defined by the user, the number that was used in the example
above are only for demonstration. The user will have to define everything in the dictionary and customize it to match
their need.
EX: name_of_dictionary = { key : value}
:return the result from the calculation based on the 2 mentioned above parameters
'''


def Hurdle_rate(segment: str, segment_hurdle_rate: dict):
    if segment is not None and segment_hurdle_rate is not None and segment in segment_hurdle_rate:
        return segment_hurdle_rate[segment]
    else:
        return None


'''
This function calculate the Hurdle Rate Compliance value based on RAROC and Hurdle Rate values
:param raroc: is the float value of RAROC
:param hurdle_rate: is the float value of Hurdle Rate
:return Yes or No string base on the comparison of the 2 values above
'''


def Hurdle_rate_compliance(raroc: float, hurdle_rate: float):
    if raroc is not None and hurdle_rate is not None:
        if raroc > hurdle_rate:
            return "Yes"
        else:
            return "No"
    else:
        if raroc is None:
            return "No"
        elif hurdle_rate is None:
            return "Yes"


'''
This function calculate the Hurdle Rate Compliance Concatenate based on Segment and Hurdle Rate Compliance strings
:param segment: is the name of Segment
:param hurdle_rate_compliance: is the string value of Hurdle Rate Compliance
:return the concatenate string between the Segment and Hurdle Rate Compliance
'''


def Hurdle_rate_compliance_concatenate(segment: str, hurdle_rate_compliance: str):
    if segment is not None and hurdle_rate_compliance is not None:
        return segment + " " + hurdle_rate_compliance
    else:
        if segment is None and hurdle_rate_compliance is not None:
            return hurdle_rate_compliance
        elif segment is not None and hurdle_rate_compliance is None:
            return segment
        else:
            return ""


def Hurdle_rate_compliance_yes(hurdle_rate_compliance_concatenate: str):
    """
    This function is to find if hurdle rate compliance concatenate contain "Yes"
    :param hurdle_rate_compliance_concatenate: the string where the word "Yes" will be search from
    :return: the "Yes" if it is found
    """
    if hurdle_rate_compliance_concatenate is not None and hurdle_rate_compliance_concatenate != "":
        if "Yes" in hurdle_rate_compliance_concatenate:
            return "Yes"
        else:
            return ""
    else:
        return ""


def Hurdle_rate_compliance_no(hurdle_rate_compliance_concatenate: str):
    """
    This function is to find if hurdle rate compliance concatenate contain "No"
    :param hurdle_rate_compliance_concatenate: the string where the word "No" will be search from
    :return: the "No" if it is found
    """
    if hurdle_rate_compliance_concatenate is not None and hurdle_rate_compliance_concatenate != "":
        if "No" in hurdle_rate_compliance_concatenate:
            return "No"
        else:
            return ""
    else:
        return ""
