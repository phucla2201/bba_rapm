import unittest, datetime
import stmo_rapm_step1 as ra

FCU_basel = {
    'Commercial Real Estate': 'HVCRE',
    'Accommodation and Food Services': 'Corporate',
    'Broker Mortgage': 'RRE',
    'Dealer Loan': 'Retail',
    'HELOC': 'RRE',
    'Industrial': 'Corporate',
    'Other Personal Loans': 'Retail',
    'Third Party Mortgages': 'RRE',
    'Residential Mortgage': 'RRE',
    'Retail Revolving LOC': 'QRRE',
    'SME': 'SME'
}

Rating_mean_nums = {
    324: 0.37960,
    374: 0.23304,
    411: 0.17635,
    437: 0.13110,
    464: 0.09611,
    488: 0.06970,
    514: 0.05015,
    538: 0.03587,
    563: 0.0255,
    589: 0.01814,
    609: 0.01285,
    629: 0.00909,
    651: 0.00642,
    671: 0.00454,
    687: 0.00320,
    701: 0.00226,
    715: 0.00159,
    726: 0.00112,
    737: 0.00079,
    751: 0.00056,
    765: 0.00039,
    779: 0.00028,
    808: 0.00020,
    863: 0.00014,
    882: 0.00010
}

Rating_mean_str = {
    "C1": 0.0001,
    "C2": 0.0005,
    "C3": 0.0010,
    "C4": 0.0023,
    "C5": 0.0050,
    "C6": 0.0111,
    "C7": 0.0243,
    "C8": 0.0526,
    "C9": 0.1100,
    "C10": 0.3796
}

Senior_rate = {
    'Senior Unsecured': 0.45,
    'Senior Secured, Collateral - Physical': 0.40,
    'Senior Secured, Collateral - Real Estate (Residential or Commercial)': 0.30,
    'Senior Secured, Collateral - Receivables': 0.35,
    'Subordinated': 0.75
}

exposure_pct = {
    'Exposure I - On Balance Sheet': 1,
    'Exposure I - Off Balance Sheet': 1,
    'Exposure II': 0.5,
    'Exposure III': 0.2,
    'Exposure IV': 0.75,
    'Exposure V': 1
}

mins_cost_of_fund = {
    0: 0,
    0.5: 0.001,
    1: 0.002,
    3: 0.0025,
    5: 0.003,
    7: 0.0035,
    10: 0.0045,
    15: 0.0055,
}

mins_capital_scaling_factor = {
    0: 1,
    0.5: 1,
    1: 1,
    3: 1,
    5: 1.25,
    7: 1.25,
    10: 1.4,
    15: 1.4,
}

segment_hurdle_rate = {
    'Commercial Real Estate': 0.16,
    'Accommodation and Food Services': 0.18,
    'Broker Mortgage': 0.12,
    'Dealer Loan': 0.12,
    'HELOC': 0.13,
    'Industrial': 0.1,
    'Other Personal Loans': 0.1,
    'Third Party Mortgages': 0.18,
    'Residential Mortgage': 0.15,
    'Retail Revolving LOC': 0.19,
    'SME': 0.15
}

arr = [2254.98, 0, 22549.32, 0, 563.7, 0, 0, 0, 0, 20290.89]

x = datetime.datetime(2019, 3, 31)

y = datetime.datetime(2026, 6, 4)


class TestRapm(unittest.TestCase):

    def test_asset_class_map(self):
        self.assertEqual(ra.asset_class_map("Retail Revolving LOC", FCU_basel), "QRRE")
        self.assertEqual(ra.asset_class_map(None, FCU_basel), "")
        self.assertEqual(ra.asset_class_map("", FCU_basel), "")
        self.assertEqual(ra.asset_class_map("A", FCU_basel), "")
        self.assertEqual(ra.asset_class_map("Retail Revolving LOC", None), "")


    def test_pd_mapping_nums(self):
        self.assertAlmostEqual(ra.PD_mapping_nums(800, Rating_mean_nums), 0.00020)
        self.assertAlmostEqual(ra.PD_mapping_nums(695, Rating_mean_nums), 0.00226)
        self.assertAlmostEqual(ra.PD_mapping_nums(0, Rating_mean_nums), 0.0005)
        self.assertAlmostEqual(ra.PD_mapping_nums(None, Rating_mean_nums), 0.0005)

    def test_pd_mapping_str(self):
        self.assertAlmostEqual(ra.PD_mapping_str("C4", Rating_mean_str), 0.0023)
        self.assertAlmostEqual(ra.PD_mapping_str("C40", Rating_mean_str), 0.0005)
        self.assertAlmostEqual(ra.PD_mapping_str(None, Rating_mean_str), 0.0005)

    def test_LGD(self):
        self.assertEqual(ra.LGD("Subordinated", Senior_rate), 0.75)
        self.assertEqual(ra.LGD("", Senior_rate), "")
        self.assertEqual(ra.LGD(None, Senior_rate), "")
        self.assertEqual(ra.LGD("Subordi", Senior_rate), "")

    def test_Correlation(self):
        self.assertAlmostEqual(ra.Correlation("QRRE", 0.0032), 0.04)
        self.assertAlmostEqual(ra.Correlation("", 0.0032), 0.04)
        self.assertAlmostEqual(ra.Correlation(None, 0.0032), 0.04)
        self.assertAlmostEqual(ra.Correlation("Corporation", None), 0.04)
        self.assertAlmostEqual(ra.Correlation(None, None), 0.04)
        self.assertAlmostEqual(ra.Correlation("RRE", 0.0032), 0.15)
        self.assertAlmostEqual(ra.Correlation("asfhowet", 0.0032), 0.04)

    def test_Maturity_Adjustment(self):
        self.assertAlmostEqual(ra.Maturity_adjustment(0.00320), 0.1876704)
        self.assertEqual(ra.Maturity_adjustment(0), "")
        self.assertEqual(ra.Maturity_adjustment(-1), "")
        self.assertEqual(ra.Maturity_adjustment(None), "")

    def test_Expected_Loss(self):
        self.assertAlmostEqual(ra.Expected_loss(0.00320, 0.45), 0.00144)
        self.assertAlmostEqual(ra.Expected_loss(None, 0.45), 0)
        self.assertAlmostEqual(ra.Expected_loss(0.00320, None), 0)
        self.assertAlmostEqual(ra.Expected_loss(None, None), 0)

    def test_EL(self):
        self.assertAlmostEqual(ra.EL(0.0014402, 2377.49), 3.42)
        self.assertEqual(ra.EL(None, 2377.49), None)
        self.assertEqual(ra.EL(0.0014402, None), None)
        self.assertEqual(ra.EL(None, None), None)

    def test_G_PD(self):
        self.assertAlmostEqual(ra.G_PD(0.0032), -2.7265513)
        self.assertEqual(ra.G_PD(None), "")
        self.assertEqual(ra.G_PD(-0.0032), "")

    def test_Maturity_Adjustment_Ecap(self):
        self.assertAlmostEqual(ra.Maturity_Adjustment_Ecap(0.1876, 2.5), 1)
        self.assertEqual(ra.Maturity_Adjustment_Ecap(None, 2.5), "")
        self.assertEqual(ra.Maturity_Adjustment_Ecap(0.1876, None), "")
        self.assertEqual(ra.Maturity_Adjustment_Ecap(None, None), "")

    def test_N(self):
        self.assertAlmostEqual(ra.N(-2.726, 0.04), 0.01316603)
        self.assertEqual(ra.N(None, 0.04), "")
        self.assertEqual(ra.N(-2.726, None), "")
        self.assertEqual(ra.N(None, None), "")

    def test_VaR(self):
        self.assertAlmostEqual(ra.VaR(0.01314, 0.45), 0.005913000)
        self.assertEqual(ra.VaR(None, 0.45), "")
        self.assertEqual(ra.VaR(0.01314, None), "")
        self.assertEqual(ra.VaR(None, None), "")

    def test_Ecap_pct(self):
        self.assertAlmostEqual(ra.Ecap_pct(0.0059163, 0.0014402), 0.0044761)
        self.assertEqual(ra.Ecap_pct(None, 0.0014402), "")
        self.assertEqual(ra.Ecap_pct(0.0059163, None), "")
        self.assertEqual(ra.Ecap_pct(None, None), "")
        self.assertEqual(ra.Ecap_pct(0.0059163, 0), "")
        self.assertEqual(ra.Ecap_pct(0, 0.0014402), "")
        self.assertEqual(ra.Ecap_pct(0, 0), "")

    def test_Ecap(self):
        self.assertAlmostEqual(ra.Ecap(0.004476, 2377.49), 10.64)
        self.assertEqual(ra.Ecap(None, 2377.49), None)
        self.assertEqual(ra.Ecap(0.004476, None), None)
        self.assertEqual(ra.Ecap(None, None), None)

    def test_Adjustment_Factor(self):
        self.assertAlmostEqual(ra.Adjustment_Factor(1, 0.187632), 1.391687727)
        self.assertEqual(ra.Adjustment_Factor(1, None), 1)
        self.assertEqual(ra.Adjustment_Factor(None, 0.187632), 1)
        self.assertEqual(ra.Adjustment_Factor(None, None), 1)

    def test_MtM_Capital_pct(self):
        self.assertAlmostEqual(ra.MtM_Capital_pct("QRRE", 0.004476, 1.3917783), 0.0062296)
        self.assertAlmostEqual(ra.MtM_Capital_pct("RRE", 0.004476, 1.3917783), 0.004476)
        self.assertAlmostEqual(ra.MtM_Capital_pct("", 0.004476, 1.3917783), 0.0062296)
        self.assertEqual(ra.MtM_Capital_pct("QRRE", None, 1.3917783), 0)
        self.assertEqual(ra.MtM_Capital_pct("QRRE", 0.004476, None), 0)
        self.assertEqual(ra.MtM_Capital_pct("QRRE", None, None), 0)

    def test_MtM_Captial(self):
        self.assertAlmostEqual(ra.MtM_Capital(0.0062296, 2377.49), 14.81)
        self.assertEqual(ra.MtM_Capital(0.0062296, None), None)
        self.assertEqual(ra.MtM_Capital(None, 2377.49), None)
        self.assertEqual(ra.MtM_Capital(None, None), None)

    def test_Granularity(self):
        self.assertAlmostEqual(ra.Granularity(0.0062296, 0.0014402), 2.47331562)
        self.assertEqual(ra.Granularity(None, 0.0014402), 0)
        self.assertEqual(ra.Granularity(0.0062296, None), 0)
        self.assertEqual(ra.Granularity(0, 0.0014402), 0)

    def test_HHI(self):
        emp = []
        self.assertAlmostEqual(ra.HHI(arr), 0.4439869)
        self.assertEqual(ra.HHI(emp), 0)
        self.assertEqual(ra.HHI(None), 0)

    def test_MtM_plus_GA_Capital_pct(self):
        self.assertAlmostEqual(ra.MtM_plus_GA_Capital_pct(0.0062296, 0.45, 2.4733, 0.001), 0.007342585)
        self.assertEqual(ra.MtM_plus_GA_Capital_pct(None, 0.45, 2.4733, 0.001), "")
        self.assertEqual(ra.MtM_plus_GA_Capital_pct(0.0062296, None, 2.4733, 0.001), "0.0062296")
        self.assertEqual(ra.MtM_plus_GA_Capital_pct(0.0062296, 0.45, None, 0.001), "0.0062296")
        self.assertEqual(ra.MtM_plus_GA_Capital_pct(0.0062296, 0.45, 2.4733, None), "0.0062296")
        self.assertEqual(ra.MtM_plus_GA_Capital_pct(None, 0.45, 2.4733, None), "")

    def test_MtM_plus_GA_Capital(self):
        self.assertAlmostEqual(ra.MtM_plus_GA_Captial(0.0073, 2377.49), 17.36)
        self.assertEqual(ra.MtM_plus_GA_Captial(None, None), None)
        self.assertEqual(ra.MtM_plus_GA_Captial(None, 2377.49), None)
        self.assertEqual(ra.MtM_plus_GA_Captial(0.0073, None), None)

    def test_Remaining_Maturity(self):
        self.assertAlmostEqual(ra.Remaining_Maturity(x, y), 7.18)
        self.assertEqual(ra.Remaining_Maturity(None, y), "")
        self.assertEqual(ra.Remaining_Maturity(x, None), "")
        self.assertEqual(ra.Remaining_Maturity(None, None), "")

    def test_Facility_Concatenate(self):
        self.assertEqual(ra.Facility_Concatenate("Exposure I - On Balance Sheet", "Yes"), "Exposure I - On Balance "
                                                                                          "Sheet Yes")
        self.assertEqual(ra.Facility_Concatenate(None, "Yes"), "Yes")
        self.assertEqual(ra.Facility_Concatenate("Exposure I - On Balance Sheet", None),
                         "Exposure I - On Balance Sheet")
        self.assertEqual(ra.Facility_Concatenate(None, None), '')

    def test_Off_balance_sheet_exposure_at_default(self):
        self.assertAlmostEqual(
            ra.Off_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet Yes", 7.18, 245.02), 122.51)
        self.assertAlmostEqual(
            ra.Off_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet Yes", None, 245.02), 49.00400000)
        self.assertEqual(
            ra.Off_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet Yes", 7.18, None), 0)
        self.assertEqual(
            ra.Off_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet Yes", None, None), 0)
        self.assertEqual(
            ra.Off_balance_sheet_exposure_at_default(None, 7.18, 245.02), 0)
        self.assertEqual(
            ra.Off_balance_sheet_exposure_at_default("Asd", 7.18, 245.02), 0)

    def test_On_balance_sheet_exposure_at_default(self):
        self.assertAlmostEqual(
            ra.On_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet", 2254.98, exposure_pct), 2254.98)
        self.assertEqual(
            ra.On_balance_sheet_exposure_at_default(None, 2254.98, exposure_pct), None)
        self.assertEqual(
            ra.On_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet", None, exposure_pct), None)
        self.assertEqual(
            ra.On_balance_sheet_exposure_at_default("Exposure I - On Balance Sheet", 2254.98, None), None)
        self.assertEqual(
            ra.On_balance_sheet_exposure_at_default("Exposure I ", 2254.98, exposure_pct), None)

    def test_Credit_Equivalent(self):
        self.assertAlmostEqual(ra.Credit_Equivalent_Credit_Risk(122.51, 2254.98), 2377.49)
        self.assertEqual(ra.Credit_Equivalent_Credit_Risk(None, None), None)
        self.assertEqual(ra.Credit_Equivalent_Credit_Risk(None, 2254.98), None)
        self.assertEqual(ra.Credit_Equivalent_Credit_Risk(122.51, None), None)

    def test_CE_Concentration_Risk(self):
        self.assertAlmostEqual(ra.CE_Concentration_Risk(17.35), 216.88)
        self.assertEqual(ra.CE_Concentration_Risk(None), None)

    def test_Operational_risk_capital(self):
        self.assertAlmostEqual(ra.Operational_Risk_Capital(348.59), 4.18)
        self.assertEqual(ra.Operational_Risk_Capital(None), None)

    def test_CE_op_risk(self):
        self.assertAlmostEqual(ra.CE_op_risk(4.18), 52.25)
        self.assertEqual(ra.CE_op_risk(None), None)

    def test_Scaled_CE(self):
        self.assertAlmostEqual(ra.Scaled_CE(2377.49, 216.81, 52.29), 2646.59)
        self.assertEqual(ra.Scaled_CE(None, 216.81, 52.29), None)
        self.assertEqual(ra.Scaled_CE(2377.49, None, 52.29), None)
        self.assertEqual(ra.Scaled_CE(2377.49, 216.81, None), None)
        self.assertEqual(ra.Scaled_CE(None, None, None), None)

    def test_Yield(self):
        self.assertAlmostEqual(ra.Yield_dollar(2377.49, 0.1466), 348.54)
        self.assertEqual(ra.Yield_dollar(None, 0.1466), None)
        self.assertEqual(ra.Yield_dollar(2377.49, None), None)
        self.assertEqual(ra.Yield_dollar(None, None), None)

    def test_Funding_cost_pct(self):
        self.assertAlmostEqual(ra.Funding_cost("Exposure I - On Balance Sheet", 7.18, mins_cost_of_fund), 0.0035)
        self.assertAlmostEqual(ra.Funding_cost(None, 7.18, mins_cost_of_fund), 0)
        self.assertAlmostEqual(ra.Funding_cost("", 7.18, mins_cost_of_fund), 0)
        self.assertAlmostEqual(ra.Funding_cost("Exposure I ", 7.18, mins_cost_of_fund), 0)
        self.assertAlmostEqual(ra.Funding_cost("Exposure I - On Balance Sheet", None, mins_cost_of_fund), 0)
        self.assertAlmostEqual(ra.Funding_cost("Exposure I - On Balance Sheet", 7.18, None), 0)

    def test_Funding_cost_dollar(self):
        self.assertAlmostEqual(round(ra.Funding_Cost_Dollar(0.0035, 2646.59), 2), 9.26)
        self.assertEqual(ra.Funding_Cost_Dollar(None, 2646.59), None)
        self.assertEqual(ra.Funding_Cost_Dollar(0.0035, None), None)

    def test_Scaled_expected_loss(self):
        self.assertAlmostEqual(ra.Scaled_Expected_Loss(2646.59, 0.0014402), 3.81)
        self.assertEqual(ra.Scaled_Expected_Loss("", ""), "")
        self.assertEqual(ra.Scaled_Expected_Loss(2646.59, None), "")
        self.assertEqual(ra.Scaled_Expected_Loss(None, 0.0014402), "")

    def test_Capital_required(self):
        self.assertAlmostEqual(ra.Capital_Required(2646.59), 211.73)
        self.assertEqual(ra.Capital_Required(0), "")
        self.assertEqual(ra.Capital_Required(None), "")

    def test_Capital_required_scaled(self):
        self.assertAlmostEqual(ra.Capital_Required_Scaled("Yes", 7.18, 211.73, mins_capital_scaling_factor), 264.66)
        self.assertAlmostEqual(ra.Capital_Required_Scaled("No", 7.18, 211.73, mins_capital_scaling_factor), 211.73)
        self.assertEqual(ra.Capital_Required_Scaled(None, 7.18, 211.73, mins_capital_scaling_factor), "")
        self.assertEqual(ra.Capital_Required_Scaled("No", None, 211.73, mins_capital_scaling_factor), "")
        self.assertEqual(ra.Capital_Required_Scaled("No", 7.18, None, mins_capital_scaling_factor), "")
        self.assertEqual(ra.Capital_Required_Scaled("No", 7.18, 211.73, None), "")

    def test_Capital_cost(self):
        self.assertAlmostEqual(ra.Capital_cost(264.66, 0.19), 50.29)
        self.assertEqual(ra.Capital_cost("", 0.19), "")
        self.assertEqual(ra.Capital_cost(264.66, ""), "")
        self.assertEqual(ra.Capital_cost(None, 0.19), "")
        self.assertEqual(ra.Capital_cost(264.66, None), "")

    def test_Risk_adjusted_margin(self):
        self.assertAlmostEqual(ra.Risk_adjusted_margin(348.59, 9.29, 3.81, 50.29, 54.1), 231.1)
        self.assertEqual(ra.Risk_adjusted_margin(None, 9.29, 3.81, 50.29, 54.1), "")
        self.assertEqual(ra.Risk_adjusted_margin(348.59, None, 3.81, 50.29, 54.1), "")
        self.assertEqual(ra.Risk_adjusted_margin(348.59, 9.29, None, 50.29, 54.1), "")
        self.assertEqual(ra.Risk_adjusted_margin(348.59, 9.29, 3.81, None, 54.1), "")
        self.assertEqual(ra.Risk_adjusted_margin(348.59, 9.29, 3.81, 50.29, None), "")

    def test_RAROC(self):
        self.assertAlmostEqual(ra.RAROC(285.23, 264.66), 1.07772236)
        self.assertAlmostEqual(ra.RAROC(None, 264.66), 0)
        self.assertAlmostEqual(ra.RAROC(285.23, None), 0)

    def test_Hurdle_rate(self):
        self.assertAlmostEqual(ra.Hurdle_rate("Retail Revolving LOC", segment_hurdle_rate), 0.19)
        self.assertEqual(ra.Hurdle_rate(None, segment_hurdle_rate), None)
        self.assertAlmostEqual(ra.Hurdle_rate("Retail Revolving LOC", None), None)
        self.assertAlmostEqual(ra.Hurdle_rate("asge", segment_hurdle_rate), None)

    def test_Hurdle_rate_compliance(self):
        self.assertEqual(ra.Hurdle_rate_compliance(1.0777, 0.19), "Yes")
        self.assertEqual(ra.Hurdle_rate_compliance(0.17, 0.19), "No")
        self.assertEqual(ra.Hurdle_rate_compliance(None, 0.19), "No")
        self.assertEqual(ra.Hurdle_rate_compliance(1.0777, None), "Yes")

    def test_Hurdle_rate_compliance_concatenate(self):
        self.assertEqual(ra.Hurdle_rate_compliance_concatenate("Retail Revolving LOC", "Yes"),
                         "Retail Revolving LOC Yes")
        self.assertEqual(ra.Hurdle_rate_compliance_concatenate(None, "Yes"),
                         "Yes")
        self.assertEqual(ra.Hurdle_rate_compliance_concatenate("Retail Revolving LOC", None),
                         "Retail Revolving LOC")
        self.assertEqual(ra.Hurdle_rate_compliance_concatenate(None, None),
                         "")

    def test_Hurdle_rate_compliance_yes(self):
        self.assertEqual(ra.Hurdle_rate_compliance_yes("Retail Revolving LOC Yes"), "Yes")
        self.assertEqual(ra.Hurdle_rate_compliance_yes(None), "")
        self.assertEqual(ra.Hurdle_rate_compliance_yes("Retail Revolving LOC a"), "")

    def test_Hurdle_rate_compliance_no(self):
        self.assertEqual(ra.Hurdle_rate_compliance_no("Retail Revolving LOC No"), "No")
        self.assertEqual(ra.Hurdle_rate_compliance_no(None), "")
        self.assertEqual(ra.Hurdle_rate_compliance_no("Retail Revolving LOC a"), "")

if __name__ == '__main__':
    unittest.main()
