

COL_NAME_STEP1__ACCOUNT_NUM = 'account_num'
COL_NAME_STEP1__POSTAL_CODE = 'postal_code'
COL_NAME_STEP1__PORTFOLIO = 'portfolio'
COL_NAME_STEP1__SEGMENT = 'segment'
COL_NAME_STEP1__RATING_GRADE = 'rating_grade'
COL_NAME_STEP1__OUTSTANDING = 'outstanding'
COL_NAME_STEP1__COMMITTED = 'committed'
COL_NAME_STEP1__UNDRAWN_AMOUNT = "undrawn_amout"
COL_NAME_STEP1__EXPIRY = 'exp'
COL_NAME_STEP1__FACILITY_TYPE = "facility_type"
COL_NAME_STEP1__FACILITY_DEFINITION = "facility_def"
COL_NAME_STEP1__YIELD_PCT = "yield_pct"


STEP1_COL_DICT = {COL_NAME_STEP1__ACCOUNT_NUM: 'AccountNum',
                  COL_NAME_STEP1__POSTAL_CODE: 'Postal_Code',
                  COL_NAME_STEP1__PORTFOLIO: 'Portfolio',
                  COL_NAME_STEP1__SEGMENT: 'Segment',
                  COL_NAME_STEP1__RATING_GRADE: 'Rating Grade',
                  COL_NAME_STEP1__COMMITTED: 'Committed',
                  COL_NAME_STEP1__OUTSTANDING: 'Outstanding',
                  COL_NAME_STEP1__UNDRAWN_AMOUNT: 'Undrawn_Amount',
                  COL_NAME_STEP1__EXPIRY: 'Expiry',
                  COL_NAME_STEP1__FACILITY_TYPE: 'Facility_Type',
                  COL_NAME_STEP1__FACILITY_DEFINITION: 'Facility_Definition',
                  COL_NAME_STEP1__YIELD_PCT: 'Yield %'
                  }

STEP1_COL_LIST = {COL_NAME_STEP1__ACCOUNT_NUM,
                  COL_NAME_STEP1__POSTAL_CODE,
                  COL_NAME_STEP1__PORTFOLIO,
                  COL_NAME_STEP1__SEGMENT,
                  COL_NAME_STEP1__RATING_GRADE,
                  COL_NAME_STEP1__COMMITTED,
                  COL_NAME_STEP1__OUTSTANDING,
                  COL_NAME_STEP1__UNDRAWN_AMOUNT,
                  COL_NAME_STEP1__EXPIRY,
                  COL_NAME_STEP1__FACILITY_TYPE,
                  COL_NAME_STEP1__FACILITY_DEFINITION,
                  COL_NAME_STEP1__YIELD_PCT
                  }

STEP2_COL__ASSET_CLASS = 'asset_class'
STEP2_COL__PD_MAPPING = 'pd_mapping'
STEP2_COL__LGD = 'lgd'
STEP2_COL__MATURITY = 'maturity'
STEP2_COL__CORRELATION = 'correlation'
STEP2_COL__MATURITY_ADJUSTMENT = 'maturity_adjustment'
STEP2_COL__EXPECTED_LOSS = 'expected_loss'
STEP2_COL__EL = 'el'
STEP2_COL__G_PD = 'g_pd'
STEP2_COL__MATURITY_ADJUSTMENT_ECAP = 'maturity_adjustment_ecap'
STEP2_COL__N = 'n'
STEP2_COL__VA_R = 'va_r'
STEP2_COL__ECAP_PCT = 'ecap_pct'
STEP2_COL__ECAP = 'ecap'
STEP2_COL__ADJUSTMENT_FACTOR = 'adjustment_factor'
STEP2_COL__MTM_CAPITAL_PCT = 'mtm_capital_pct'
STEP2_COL__MTM_CAPITAL = 'mtm_capital'

STEP2_COL_DICT = {STEP2_COL__ASSET_CLASS: "Asset Class mapping with Basel 2",
                  STEP2_COL__PD_MAPPING: 'PD mapping',
                  STEP2_COL__LGD: 'LGD',
                  STEP2_COL__MATURITY: 'Maturity',
                  STEP2_COL__CORRELATION: 'Correlation',
                  STEP2_COL__MATURITY_ADJUSTMENT: 'Maturity Adjustment',
                  STEP2_COL__EXPECTED_LOSS: 'Expected Loss',
                  STEP2_COL__EL: 'EL($)',
                  STEP2_COL__G_PD: 'G(PD)',
                  STEP2_COL__MATURITY_ADJUSTMENT_ECAP: 'Maturity Adjustment_Ecap',
                  STEP2_COL__N: 'N',
                  STEP2_COL__VA_R: 'VaR',
                  STEP2_COL__ECAP_PCT: 'Ecap %',
                  STEP2_COL__ECAP: 'Ecap',
                  STEP2_COL__ADJUSTMENT_FACTOR: 'Adjustment Factor',
                  STEP2_COL__MTM_CAPITAL_PCT: 'MtM Capital %',
                  STEP2_COL__MTM_CAPITAL: 'MtM Capital $'
                  }

STEP2_COL_LIST = [STEP2_COL__ASSET_CLASS,
                  STEP2_COL__PD_MAPPING,
                  STEP2_COL__LGD,
                  STEP2_COL__MATURITY,
                  STEP2_COL__CORRELATION,
                  STEP2_COL__MATURITY_ADJUSTMENT,
                  STEP2_COL__EXPECTED_LOSS,
                  STEP2_COL__EL,
                  STEP2_COL__G_PD,
                  STEP2_COL__MATURITY_ADJUSTMENT_ECAP,
                  STEP2_COL__N,
                  STEP2_COL__VA_R,
                  STEP2_COL__ECAP_PCT,
                  STEP2_COL__ECAP,
                  STEP2_COL__ADJUSTMENT_FACTOR,
                  STEP2_COL__MTM_CAPITAL_PCT,
                  STEP2_COL__MTM_CAPITAL
                  ]


STEP3_COL__GRANULARITY = "granularity"
STEP3_COL__HHI = "hhi"
STEP3_COL__MTM_AND_GA_CAPITAL_PCT = "mtm_and_ga_capital_pct"
STEP3_COL__MTM_AND_GA_CAPITAL = "mtm_and_ga_capital"
STEP3_COL__CONCENTRATION_RISK_CAPITAL = "concentration_risk_capital"
STEP3_COL__REMAINING_MATURITY = "remaining_maturity"
STEP3_COL__FACILITY_CONCATENATE = "facility_concatenate"
STEP3_COL__OFF_BALANCE_SHEET = "off_balance_sheet_exposure_at_default"
STEP3_COL__ON_BALANCE_SHEET = "on_balance_sheet_exposure_at_default"
STEP3_COL__CREDIT_EQUIVALENT = "credit_equivalent_credit_risk"
STEP3_COL__OPERATIONAL_RISK_CAPITAL = "operational_risk_capital"
STEP3_COL__CE_OP_RISK = "ce_op.risk"
STEP3_COL__SCALED_CE = "scaled_ce"
STEP3_COL__YIELD = "yield"
STEP3_COL__FUNDING_COST_PCT = "funding_cost_pct"
STEP3_COL__FUNDING_COST = "funding_cost"
STEP3_COL__SCALED_EXPECTED_LOSS = "scaled expected loss"
STEP3_COL__CAPITAL_REQUIRED = "capital_required"
STEP3_COL__CAPITAL_REQUIRED_SCALED = "capital_required_scaled"
STEP3_COL__CAPITAL_COST = "capital_cost"
STEP3_COL__RISK_ADJUSTED_RETURN = "risk_adjusted_return"
STEP3_COL__RAROC = "raroc"
STEP3_COL__HURDLE_RATE = "hurdle_rate"
STEP3_COL__HURDLE_RATE_COMPLIANCE = "hurdle_rate_compliance"
STEP3_COL__HURDLE_RATE_COMPLIANCE_CONCATENATE = "hurdle_rate_compliance_concatenate"

STEP3_COL_DICT = {
    STEP3_COL__GRANULARITY: 'Granularity',
    STEP3_COL__HHI: 'HHI',
    STEP3_COL__MTM_AND_GA_CAPITAL_PCT: 'MtM + GA Capital',
    STEP3_COL__MTM_AND_GA_CAPITAL: 'MtM + GA Capital ($)',
    STEP3_COL__CONCENTRATION_RISK_CAPITAL: 'Concentration Risk Capital ',
    STEP3_COL__REMAINING_MATURITY: "Remaining maturity (in years)",
    STEP3_COL__FACILITY_CONCATENATE: "Facility concatenate",
    STEP3_COL__OFF_BALANCE_SHEET: "Off balance sheet exposure at default",
    STEP3_COL__ON_BALANCE_SHEET: "On balance sheet exposure at default",
    STEP3_COL__CREDIT_EQUIVALENT: "Credit equivalent credit risk",
    STEP3_COL__OPERATIONAL_RISK_CAPITAL: "Operational risk capital",
    STEP3_COL__CE_OP_RISK: 'CE op.risk',
    STEP3_COL__SCALED_CE: 'Scaled CE',
    STEP3_COL__YIELD: 'Yield $',
    STEP3_COL__FUNDING_COST_PCT: 'Funding cost (%)',
    STEP3_COL__FUNDING_COST: 'Funding cost ($)',
    STEP3_COL__SCALED_EXPECTED_LOSS: 'Scale expected loss',
    STEP3_COL__CAPITAL_REQUIRED: 'Capital required',
    STEP3_COL__CAPITAL_REQUIRED_SCALED: 'Capital required scaled',
    STEP3_COL__CAPITAL_COST: 'Capital cost',
    STEP3_COL__RISK_ADJUSTED_RETURN: "Risk adjusted return",
    STEP3_COL__RAROC: "Raroc",
    STEP3_COL__HURDLE_RATE: "Hurdle rate",
    STEP3_COL__HURDLE_RATE_COMPLIANCE: "Hurdle rate compliance",
    STEP3_COL__HURDLE_RATE_COMPLIANCE_CONCATENATE: "Hurdle rate compliance concatenate"
}

STEP3_COL_LIST = [
    STEP3_COL__GRANULARITY,
    STEP3_COL__HHI,
    STEP3_COL__MTM_AND_GA_CAPITAL_PCT,
    STEP3_COL__MTM_AND_GA_CAPITAL,
    STEP3_COL__CONCENTRATION_RISK_CAPITAL,
    STEP3_COL__REMAINING_MATURITY,
    STEP3_COL__FACILITY_CONCATENATE,
    STEP3_COL__OFF_BALANCE_SHEET,
    STEP3_COL__ON_BALANCE_SHEET,
    STEP3_COL__CREDIT_EQUIVALENT,
    STEP3_COL__OPERATIONAL_RISK_CAPITAL,
    STEP3_COL__CE_OP_RISK,
    STEP3_COL__SCALED_CE,
    STEP3_COL__YIELD,
    STEP3_COL__FUNDING_COST_PCT,
    STEP3_COL__FUNDING_COST,
    STEP3_COL__SCALED_EXPECTED_LOSS,
    STEP3_COL__CAPITAL_REQUIRED,
    STEP3_COL__CAPITAL_REQUIRED_SCALED,
    STEP3_COL__CAPITAL_COST,
    STEP3_COL__RISK_ADJUSTED_RETURN,
    STEP3_COL__RAROC,
    STEP3_COL__HURDLE_RATE,
    STEP3_COL__HURDLE_RATE_COMPLIANCE,
    STEP3_COL__HURDLE_RATE_COMPLIANCE_CONCATENATE
]

FIX_PARA__FCU_CLASSIFICATION = "fcu_classification"
FIX_PARA__BASEL_CLASSIFICATION = "basel_classification"
FIX_PARA__CORRELATION = "correlation"
FIX_PARA__RATING = "rating"
FIX_PARA__MEAN_AVE = "mean_average"
FIX_PARA__FUNDING_COSTS = "funding_cost"
FIX_PARA__CONFIDENCE_INTERVAL = "confidence_interval"
FIX_PARA__Z_VALUE = "z_value"
FIX_PARA__EXPOSURE_TYPE = "exposure_type"
FIX_PARA__EXPOSURE_TYPE_DETAIL = "exposure_type_detail"
FIX_PARA__EXPOSURE_TYPE_PCT = "exposure_type_pct"
FIX_PARA__SEGMENTED_HURDLE = "segmented_hurdle"
FIX_PARA__SEGMENTED_HURDLE_RATE = "segmented_hurdle_rate"
FIX_PARA__SENIOR_UNSECURED = "senior_unsecured"
FIX_PARA__SENIOR_SECURED_PHY = "senior_secured_physical"
FIX_PARA__SENIOR_SECURED_REAL_ES = "senior_secured_real_estate"
FIX_PARA__SENIOR_SECURED_RECEI = "senior_secured_receivables"
FIX_PARA__SUBORDINATED = "subordinated"

FIX_PARA_DICT = {
    FIX_PARA__FCU_CLASSIFICATION: "FCU classification",
    FIX_PARA__BASEL_CLASSIFICATION: "Basel classification",
    FIX_PARA__CORRELATION: "Correlation",
    FIX_PARA__RATING: "Rating",
    FIX_PARA__MEAN_AVE: "Mean average",
    FIX_PARA__FUNDING_COSTS: "Funding cost",
    FIX_PARA__CONFIDENCE_INTERVAL: "Confidence interval",
    FIX_PARA__Z_VALUE: "Z value",
    FIX_PARA__EXPOSURE_TYPE: "Exposure type",
    FIX_PARA__EXPOSURE_TYPE_DETAIL: "Exposure type detail",
    FIX_PARA__EXPOSURE_TYPE_PCT: "Exposure type %",
    FIX_PARA__SEGMENTED_HURDLE: "Segmented hurdle",
    FIX_PARA__SEGMENTED_HURDLE_RATE: "Segmented hurdle rate",
    FIX_PARA__SENIOR_UNSECURED: "45%",
    FIX_PARA__SENIOR_SECURED_PHY: "40%",
    FIX_PARA__SENIOR_SECURED_REAL_ES: "30%",
    FIX_PARA__SENIOR_SECURED_RECEI: "35%",
    FIX_PARA__SUBORDINATED: "75%",
}

FIX_PARA__LIST = [
    FIX_PARA__FCU_CLASSIFICATION,
    FIX_PARA__BASEL_CLASSIFICATION,
    FIX_PARA__CORRELATION,
    FIX_PARA__RATING,
    FIX_PARA__MEAN_AVE,
    FIX_PARA__FUNDING_COSTS,
    FIX_PARA__CONFIDENCE_INTERVAL,
    FIX_PARA__Z_VALUE,
    FIX_PARA__EXPOSURE_TYPE,
    FIX_PARA__EXPOSURE_TYPE_DETAIL,
    FIX_PARA__EXPOSURE_TYPE_PCT,
    FIX_PARA__SEGMENTED_HURDLE,
    FIX_PARA__SEGMENTED_HURDLE_RATE,
    FIX_PARA__SENIOR_UNSECURED,
    FIX_PARA__SENIOR_SECURED_PHY,
    FIX_PARA__SENIOR_SECURED_REAL_ES,
    FIX_PARA__SENIOR_SECURED_RECEI,
    FIX_PARA__SUBORDINATED
]
'''STEP4A_COL__PORTFOLIO = 'portfolio'
STEP4A_COL__SEGMENT = 'segment'
STEP4A_COL__EAD = 'ead'
STEP4A_COL__ECAP = 'ecap'
STEP4A_COL_DICT = {
    STEP4A_COL__PORTFOLIO: 'Portfolio',
    STEP4A_COL__SEGMENT: 'Segment',
    STEP4A_COL__EAD: 'Exposure at Default ($)',
    STEP4A_COL__ECAP: 'Economic Capital ($)',
}
STEP4A_COL_LIST = [
    STEP4A_COL__PORTFOLIO,
    STEP4A_COL__SEGMENT,
    STEP4A_COL__EAD,
    STEP4A_COL__ECAP,
]

STEP4B_COL__PORTFOLIO = 'portfolio'
STEP4B_COL__SEGMENT = 'segment'
STEP4B_COL__EAD = 'ead'
STEP4B_COL__MTM_CAPITAL = 'mtm_capital'
STEP4B_COL__MTM_RELAXED = 'mtm_relaxed'
STEP4B_COL__CONCENTRATION_CHARGE = 'concentration_charge'
STEP4B_COL_DICT = {
    STEP4B_COL__PORTFOLIO: "Portfolio",
    STEP4B_COL__SEGMENT: 'Segment',
    STEP4B_COL__EAD: 'Exposure at Default ($)',
    STEP4B_COL__MTM_CAPITAL: 'MtM Capital with Granularity Assumption ($)',
    STEP4B_COL__MTM_RELAXED: 'MtM with Granularity Assumption Relaxed ($)',
    STEP4B_COL__CONCENTRATION_CHARGE: 'Concentration Charge ($)'
}
STEP4B_COL_LIST = [
    STEP4B_COL__PORTFOLIO,
    STEP4B_COL__SEGMENT,
    STEP4B_COL__EAD,
    STEP4B_COL__MTM_CAPITAL,
    STEP4B_COL__MTM_RELAXED,
    STEP4B_COL__CONCENTRATION_CHARGE
]'''
